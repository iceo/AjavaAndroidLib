package com.zftlive.android.library.common;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.zftlive.android.library.base.BaseActivity;

/**
 * 展示本地/服务器网页共通界面
 * @author 曾繁添
 * @version 1.0
 *
 */
public class WebPageActivity extends BaseActivity {

	@Override
	public int bindLayout() {
		return 0;
	}

	@Override
	public View bindView() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void initParms(Bundle parms) {
		// TODO Auto-generated method stub

	}

	@Override
	public void initView(View view) {
		// TODO Auto-generated method stub

	}

	@Override
	public void doBusiness(Context mContext) {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

}
